﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace mart.io.DesktopShareClientDotNet.Models
{
    public class UpdateScreenShotRequest
    {
        public Bitmap ScreenShot { get; set; }
        public Rectangle BoundingRectangle { get; set; }
        public bool IsInitialScreenshot { get; set; }
    }
}
