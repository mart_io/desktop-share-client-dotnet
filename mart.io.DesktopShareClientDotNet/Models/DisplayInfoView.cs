﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace mart.io.DesktopShareClientDotNet.Models
{
    public class DisplayInfoView
    {
        public int ItemIndex { get; set; }
        public Screen Screen { get; set; }
    }
}
