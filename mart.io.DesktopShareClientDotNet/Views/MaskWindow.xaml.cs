﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace mart.io.DesktopShareClientDotNet
{
    /// <summary>
    /// Interaction logic for MaskWindow.xaml
    /// </summary>
    public partial class MaskWindow : Window
    {
        public event EventHandler SelectionChanged;
        public Rectangle SelectionArea { get; set; }
        public Point SelectionTopLeft { get; set; }

        public MaskWindow()
        {
            InitializeComponent();
        }

        bool mouseDrag = false;
        bool mouseDown = false; // Set to 'true' when mouse is held down.
        Point mouseDownPos; // The point where the mouse button was clicked down.
        Rectangle selectionBox = null;

        protected virtual void OnSelectionChanged(EventArgs e)
        {
            EventHandler handler = SelectionChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void RevertSelectState()
        {
            mouseDrag = false;
            mouseDown = false;
            if (selectionBox != null)
            {
                canvas.Children.Remove(selectionBox);
                selectionBox = null;
            }
            theGrid.ReleaseMouseCapture();
            lblStatus.Visibility = System.Windows.Visibility.Collapsed;
            this.Hide();
            Mouse.OverrideCursor = Cursors.Arrow;
        }

        private void PopupCaptureWindow()
        {
            theGrid.ReleaseMouseCapture();

            SelectionTopLeft = selectionBox.PointToScreen(new Point(0, 0));
            SelectionArea = selectionBox;

            RevertSelectState();
            OnSelectionChanged(EventArgs.Empty);
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.RightButton == MouseButtonState.Pressed)
            {
                RevertSelectState();
                return;
            }
            else if (e.LeftButton == MouseButtonState.Pressed && e.ClickCount > 1)
            {
                return;
            }
            else if (e.LeftButton == MouseButtonState.Pressed && mouseDown)
            {
                // mouse was already clicked once before and it wasn't dragged ..so treat second click as second point of rectangle
                Point mousePos = e.GetPosition(theGrid);
                if (Math.Abs(mouseDownPos.X - mousePos.X) < 10 || Math.Abs(mouseDownPos.Y - mousePos.Y) < 10)
                {
                    // figure out how to cancel popping up the window
                }
                PopupCaptureWindow();
            }
            else if (e.LeftButton == MouseButtonState.Pressed) // this is the first time we've clicked on the canvas ..this could be either part of a mouse drag or a second click 
            {
                // Make the drag selection box visible.
                selectionBox = new Rectangle() { Stroke = Brushes.Black, StrokeThickness = 2 };
                selectionBox.SizeChanged += selectionBox_SizeChanged;
                canvas.Children.Add(selectionBox);

                Mouse.OverrideCursor = Cursors.Cross;
                mouseDown = true;
                mouseDownPos = e.GetPosition(null);
                theGrid.CaptureMouse();
                // Initial placement of the drag selection box.         
                Canvas.SetLeft(selectionBox, mouseDownPos.X);
                Canvas.SetTop(selectionBox, mouseDownPos.Y);
            }
        }

        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (mouseDrag)
            {
                PopupCaptureWindow();
            }
        }

        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed || mouseDown)
            {
                // When the mouse is held down, reposition the drag selection box.

                Point mousePos = e.GetPosition(theGrid);

                if (mouseDownPos.X < mousePos.X)
                {
                    Canvas.SetLeft(selectionBox, mouseDownPos.X);
                    selectionBox.Width = mousePos.X - mouseDownPos.X;
                }
                else
                {
                    Canvas.SetLeft(selectionBox, mousePos.X);
                    selectionBox.Width = mouseDownPos.X - mousePos.X;
                }

                if (mouseDownPos.Y < mousePos.Y)
                {
                    Canvas.SetTop(selectionBox, mouseDownPos.Y);
                    selectionBox.Height = mousePos.Y - mouseDownPos.Y;
                }
                else
                {
                    Canvas.SetTop(selectionBox, mousePos.Y);
                    selectionBox.Height = mouseDownPos.Y - mousePos.Y;
                }

                mouseDrag = true;
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.Close();
            }
        }

        private void selectionBox_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            lblStatus.Visibility = System.Windows.Visibility.Visible;
            if (double.IsNaN(selectionBox.Width) || double.IsNaN(selectionBox.Height)) return;
            lblStatus.Content = string.Format("({0}x{1})", selectionBox.Width, selectionBox.Height);
        }
    }
}
