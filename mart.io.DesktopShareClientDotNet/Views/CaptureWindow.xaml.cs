﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace mart.io.DesktopShareClientDotNet
{
    /// <summary>
    /// Interaction logic for CaptureWindow.xaml
    /// </summary>
    public partial class CaptureWindow : Window
    {
        public CaptureWindow()
        {
            InitializeComponent();
            Loaded += new RoutedEventHandler(CaptureWindow_Loaded);
        }

        private void CaptureWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Border titleBar = this.Template.FindName("PART_TITLEBAR", this) as Border;
            if (titleBar != null)
            {
                titleBar.MouseLeftButtonDown += PART_TITLEBAR_MouseLeftButtonDown;
                Button btnClose = titleBar.FindName("PART_CLOSE") as Button;
                if (btnClose != null)
                {
                    btnClose.Click += PART_CLOSE_Click;
                }
            }
        }

        private void PART_TITLEBAR_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void PART_CLOSE_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }
    }
}
