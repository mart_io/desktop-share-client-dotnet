﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using mart.io.DesktopShareClientDotNet.Util;

namespace mart.io.DesktopShareClientDotNet.Utils
{
    public static class Win32WindowManagementUtility
    {
        private const int SW_SHOWNORMAL = 1;
        private const int SW_SHOWMAXIMIZED = 3;
        private const int SW_RESTORE = 9;
        private const int GWL_STYLE = -16;
        private const UInt32 WS_MINIMIZE = 0x20000000;

        public static void RestoreWindow(IntPtr hwnd)
        {
            int style = Win32Imports.GetWindowLong(hwnd, GWL_STYLE);
            if ((style & WS_MINIMIZE) == WS_MINIMIZE)
            {
                Win32Imports.ShowWindow(hwnd, SW_SHOWNORMAL);
                Win32Imports.ShowWindow(hwnd, SW_RESTORE);
                System.Windows.Application.Current.MainWindow.Activate();
            }
        }

        public static Bitmap PrintWindow(IntPtr hwnd)
        {
            RECT rc;
            Win32Imports.GetWindowRect(hwnd, out rc);

            if (rc.Width == 0 || rc.Height == 0)
            {
                return null;
            }

            Bitmap bmp = new Bitmap(rc.Width, rc.Height, PixelFormat.Format32bppArgb);
            Graphics gfxBmp = Graphics.FromImage(bmp);
            IntPtr hdcBitmap = gfxBmp.GetHdc();

            Win32Imports.PrintWindow(hwnd, hdcBitmap, 0);

            gfxBmp.ReleaseHdc(hdcBitmap);
            gfxBmp.Dispose();

            return bmp;
        }
    }
}
