﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Compression;

namespace mart.io.DesktopShareClientDotNet.Util
{
    public static class GZipCompression
    {
        public static byte[] Compress(byte[] inputData)
        {
            if (inputData == null) throw new ArgumentNullException("inputData");

            using (var ms = new MemoryStream())
            {
                using (GZipStream gzip = new GZipStream(ms, CompressionMode.Compress))
                {
                    gzip.Write(inputData, 0, inputData.Length);
                }
                return ms.ToArray();
            }
        }

        public static byte[] Decompress(byte[] inputData)
        {
            if (inputData == null) throw new ArgumentNullException("inputData");

            using (var ms = new MemoryStream(inputData))
            {
                using (var decompressedMs = new MemoryStream())
                {
                    using (GZipStream gzip = new GZipStream(ms, CompressionMode.Decompress)) 
                    {
                        gzip.CopyTo(decompressedMs);
                    }
                    return decompressedMs.ToArray();
                }
            }
        }
    }
}
