﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using mart.io.DesktopShareClientDotNet.Interfaces;
using System.IO;
using System.Drawing.Imaging;

namespace mart.io.DesktopShareClientDotNet.Util
{
    public class PNGBitmapCompression : ICompressBitmapStrategy
    {
        public byte[] CompressBitmap(System.Drawing.Bitmap bitmap)
        {
            MemoryStream ms = new MemoryStream();
            bitmap.Save(ms, ImageFormat.Png);
            byte[] compressedBytes = ms.GetBuffer();
            return compressedBytes;
        }
    }
}
