﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.Windows.Media;
using System.Drawing.Imaging;
using System.Windows;
using System.Runtime.InteropServices;

namespace mart.io.DesktopShareClientDotNet.Utils
{
    public static class ScreenshotUtility
    {
        public static Bitmap GetScreenshot(IntPtr windowHandle)
        {
            Win32WindowManagementUtility.RestoreWindow(windowHandle);
            Bitmap screenShot = Win32WindowManagementUtility.PrintWindow(windowHandle);
            
            if (screenShot == null)
            {
                return null;
            }

            return screenShot;
        }

        public static Bitmap GetScreenshot(int x, int y, int width, int height)
        {
            if (width <= 0 || height <= 0) return null;
            Bitmap screenShot = null;

            screenShot = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage(screenShot))
            {
                g.CopyFromScreen(new System.Drawing.Point(x, y), System.Drawing.Point.Empty, screenShot.Size);
            }
            return screenShot;
        }
    }
}
