﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.Windows.Media;
using System.Drawing.Imaging;
using System.Windows;
using System.Runtime.InteropServices;
using System.IO;
using mart.io.DesktopShareClientDotNet.Util;

namespace mart.io.DesktopShareClientDotNet.Utils
{
    public static class BitmapHelpers
    {
        public static Bitmap CropBitmap(Bitmap image, Rectangle cropBounds)
        {
            Bitmap croppedImage = new Bitmap(cropBounds.Width, cropBounds.Height);
            using (Graphics graphics = Graphics.FromImage(croppedImage))
            {
                graphics.DrawImage(image, 0, 0, cropBounds, GraphicsUnit.Pixel);
            }
            return croppedImage;
        }

        public static BitmapSource RawBitmapToBitmapSource(Bitmap screenShot)
        {
            WriteableBitmap writeableBitmap = new WriteableBitmap(screenShot.Width, screenShot.Height, 96, 96, PixelFormats.Pbgra32, null);
            BitmapData data = screenShot.LockBits(new System.Drawing.Rectangle(0, 0, screenShot.Width, screenShot.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            try
            {
                writeableBitmap.Lock();
                Win32Imports.CopyMemory(writeableBitmap.BackBuffer, data.Scan0,
                    (writeableBitmap.BackBufferStride * screenShot.Height));
                writeableBitmap.AddDirtyRect(new Int32Rect(0, 0, screenShot.Width, screenShot.Height));
                writeableBitmap.Unlock();
                return writeableBitmap;
            }
            finally
            {
                screenShot.UnlockBits(data);
            }
        }

        public static byte[] BitmapToByteArray(Bitmap bitmap)
        {
            BitmapData bmpdata = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, bitmap.PixelFormat);
            byte[] bytedata = new byte[bmpdata.Stride * bitmap.Height];

            Marshal.Copy(bmpdata.Scan0, bytedata, 0, bytedata.Length);
            bitmap.UnlockBits(bmpdata);

            return bytedata;
        }

        // Windows bitmaps are in BRGA - most platforms expect it to be in RGBA eg Html5 canvas
        public static byte[] BitmapBytesToRGBA(byte[] bytedata)
        {
            byte[] rgba = new byte[bytedata.Length];
            for (int i = 0; i < bytedata.Length; i += 4)
            {
                rgba[i] = bytedata[i + 2];
                rgba[i + 1] = bytedata[i + 1];
                rgba[i + 2] = bytedata[i];
                rgba[i + 3] = bytedata[i + 3];
            }
            return rgba;
        }

        public static bool ByteArrayCompare(byte[] b1, byte[] b2)
        {
            // Validate buffers are the same length.
            // This also ensures that the count does not exceed the length of either buffer.  
            return b1.Length == b2.Length && Win32Imports.memcmp(b1, b2, b1.Length) == 0;
        }

        public static bool AreBitmapsEquivalent(Bitmap b1, Bitmap b2)
        {
            if ((b1 == null && b2 != null) || (b1 != null && b2 == null) || b1.Width != b2.Width || b1.Height != b2.Height) return false;
            return ByteArrayCompare(BitmapToByteArray(b1), BitmapToByteArray(b2));
        }

        public static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }

            return null;
        }

        public static void BitmapToStream(Bitmap bmp, Stream stream)
        {
            EncoderParameters encoderParameters = new EncoderParameters(1);
            encoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
            bmp.Save(stream, GetEncoder(ImageFormat.Jpeg), encoderParameters);
        }

        public static Bitmap CopyBitmap(Bitmap target, Rectangle bounds, Bitmap source)
        {
            using (Graphics g = Graphics.FromImage(target))
            {
                g.DrawImage(source, bounds);
                g.Flush();
            }
            return target;
        }

        public static Rectangle GetBoundingBoxForChanges(Bitmap prev, Bitmap current)
        {
            if (current == null)
            {
                throw new ArgumentNullException("current");
            }
            else if (prev == null)
            {
                return new Rectangle(0, 0, current.Width, current.Height);
            }
            else if (prev.Width != current.Width || prev.Height != current.Height || prev.PixelFormat != current.PixelFormat)
            {
                return Rectangle.Empty;
            }

            int numBytesPerPixel = 4;
            int width = current.Width;
            int height = current.Height;
            
            // calculated in first pass
            int top = height;
            int left = width;
            
            // approximated in first pass, calculated in second pass
            int right = 0;
            int bottom = 0;

            BitmapData buffer1 = null;
            BitmapData buffer2 = null;
            try
            {
                buffer1 = current.LockBits(new Rectangle(0, 0, current.Width, current.Height), ImageLockMode.ReadOnly, current.PixelFormat);
                buffer2 = prev.LockBits(new Rectangle(0, 0, prev.Width, prev.Height), ImageLockMode.ReadOnly, prev.PixelFormat);

                int stride1 = buffer1.Stride / numBytesPerPixel;
                int stride2 = buffer2.Stride / numBytesPerPixel;

                unsafe
                {
                    // cast the safe pointers into unsafe pointers
                    int* p1 = (int*)buffer1.Scan0;
                    int* p2 = (int*)buffer2.Scan0;

                    // First pass: find the left, top as well as an APPROXIMATION of bottom and right bounds of bounding rectangle
                    // Scan left-to-right only up to the left bound and find an initial bottom, right bound (this cuts down the
                    // second pass)
                    for (int y = 0; y < current.Height; y++)
                    {
                        // pixels up to the current left bound
                        for (int x = 0; x < left; x++)
                        {
                            if ((p1 + x)[0] != (p2 + x)[0])
                            {
                                // found a change in bounding area to bottom or right bound
                                if (x < left)
                                {
                                    left = x;
                                }
                                if (x > right)
                                {
                                    right = x;
                                }
                                if (y < top)
                                {
                                    top = y;
                                }
                                if (y > bottom)
                                {
                                    bottom = y;
                                }
                            }
                        }

                        p1 += stride1;
                        p2 += stride2;
                    }

                    // no changed pixels so no need for second pass
                    if (left != width)
                    {
                        // scan from bottom-right using bounds found in first pass and progress UPWARDS
                        p1 = (int*)buffer1.Scan0;
                        p2 = (int*)buffer2.Scan0;
                        p1 += (current.Height - 1) * stride1;
                        p2 += (prev.Height - 1) * stride2;

                        // for each row (bottom to top)
                        for (int y = current.Height - 1; y > top; y--)
                        {
                            // pixels up to the current right bound (right to left)
                            for (int x = current.Width - 1; x > right; x--)
                            {
                                if ((p1 + x)[0] != (p2 + x)[0])
                                {
                                    // found a change in bounding area to bottom or right bound
                                    if (x > right)
                                    {
                                        right = x;
                                    }
                                    if (y > bottom)
                                    {
                                        bottom = y;
                                    }
                                }
                            }

                            p1 -= stride1;
                            p2 -= stride2;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                if (buffer1 != null)
                {
                    current.UnlockBits(buffer1);
                }
                if (buffer2 != null)
                {
                    prev.UnlockBits(buffer2);
                }
            }

            // validate bounding box
            int diffImgWidth = right - left + 1;
            int diffImgHeight = bottom - top + 1;
            if (diffImgHeight < 0 || diffImgWidth < 0)
            {
                return Rectangle.Empty;
            }

            return new Rectangle(left, top, diffImgWidth, diffImgHeight);
        }

        public static Bitmap GetDifference(Bitmap a, Bitmap b)
        {
            Rectangle rectChanges = BitmapHelpers.GetBoundingBoxForChanges(a, b);
            if (rectChanges == System.Drawing.Rectangle.Empty)
            {
                return null;
            }
            return BitmapHelpers.CropBitmap(b, rectChanges);  // extract only the changed part
        }
    }
}
