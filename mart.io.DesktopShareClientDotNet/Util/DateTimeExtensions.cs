﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mart.io.DesktopShareClientDotNet.Utils
{
    public static class DateTimeExtensions
    {
        private static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static long ToUnixTimeTicks(this DateTime dateTime)
        {
            return (dateTime - UnixEpoch).Ticks / TimeSpan.TicksPerMillisecond;
        }
    }
}
