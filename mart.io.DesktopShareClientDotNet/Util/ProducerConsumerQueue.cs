﻿using System;
using System.Threading;
using System.Collections.Generic;

namespace mart.io.DesktopShareClientDotNet.Utils
{
    public class ProducerConsumerQueue<T>
    {
        readonly object _locker = new object();
        Thread[] _workers;
        Queue<T> _itemQ = new Queue<T>();
        Action<T> doWork = null;

        public ProducerConsumerQueue(Action<T> callback, int workerCount = 1)
        {
            doWork = callback;
            _workers = new Thread[workerCount];

            for (int i = 0; i < workerCount; i++)
                (_workers[i] = new Thread(Consume)).Start();
        }

        public int Count()
        {
            lock (_locker)
            {
                return _itemQ.Count;
            }
        }

        public void ClearQueue()
        {
            ClearQueue(null);
        }

        // in case you wanna clear, then queue an item immediately
        public void ClearQueue(Action afterClear)
        {
            lock (_locker)
            {
                _itemQ.Clear();
                if (afterClear != null)
                {
                    afterClear();
                }
            }
        }

        public void Shutdown(bool waitForWorkers)
        {
            // Enqueue one null item per worker to make each exit.
            foreach (Thread worker in _workers)
                EnqueueItem(default(T));

            if (waitForWorkers)
                foreach (Thread worker in _workers)
                    worker.Join();
        }

        public void EnqueueItem(T item)
        {
            lock (_locker)
            {
                _itemQ.Enqueue(item);
                Monitor.Pulse(_locker);
            }
        }

        void Consume()
        {
            while (true)
            {
                T item;
                lock (_locker)
                {
                    while (_itemQ.Count == 0) Monitor.Wait(_locker);
                    item = _itemQ.Dequeue();
                }
                if (item == null) return;         // This signals our exit.
                doWork(item);
            }
        }
    }
}
