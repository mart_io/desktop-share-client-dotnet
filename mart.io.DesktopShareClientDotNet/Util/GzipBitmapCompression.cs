﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using mart.io.DesktopShareClientDotNet.Interfaces;
using mart.io.DesktopShareClientDotNet.Utils;

namespace mart.io.DesktopShareClientDotNet.Util
{
    public class GzipBitmapCompression : ICompressBitmapStrategy
    {

        public byte[] CompressBitmap(System.Drawing.Bitmap bitmap)
        {
            byte[] bytes = BitmapHelpers.BitmapBytesToRGBA(BitmapHelpers.BitmapToByteArray(bitmap));
            byte[] compressedBytes = GZipCompression.Compress(bytes);
            return compressedBytes;
        }
    }
}
