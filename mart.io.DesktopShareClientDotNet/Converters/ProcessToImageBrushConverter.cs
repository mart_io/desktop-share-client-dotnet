﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;
using System.Diagnostics;
using mart.io.DesktopShareClientDotNet.Utils;
using System.Windows.Media;

namespace mart.io.DesktopShareClientDotNet.Converters
{
    public class ProcessToImageBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            Process p = value as Process;
            if (p== null) return null;

            return BitmapHelpers.RawBitmapToBitmapSource(ScreenshotUtility.GetScreenshot(p.MainWindowHandle));
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
