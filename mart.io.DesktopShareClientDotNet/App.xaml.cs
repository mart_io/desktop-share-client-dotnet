﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel; 
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace mart.io.DesktopShareClientDotNet
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            DesktopShareBootstrapper bootstrapper = new DesktopShareBootstrapper();
            bootstrapper.Run();
        }
    }
}
