﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using mart.io.DesktopShareClientDotNet.Utils;
using mart.io.DesktopShareClientDotNet.Models;
using System.Net;
using System.IO;
using mart.io.DesktopShareClientDotNet.Util;
using System.Drawing.Imaging;
using mart.io.DesktopShareClientDotNet.Interfaces;
using System.Configuration;

namespace mart.io.DesktopShareClientDotNet.Services
{
    public class ScreenShotErrorEventArgs : EventArgs
    {
        public ScreenShotErrorEventArgs() { }

        public ScreenShotErrorEventArgs(string error)
        {
            Error = error;
        }

        public string Error { get; set; }
    }

    public class ScreenShotChangedEventArgs : EventArgs
    {
        public int SequenceNumber { get; set; }

        public Bitmap CurrentScreenShot { get; set; }
        
        public Bitmap CurrentScreenShotDelta { get; set; }

        public ScreenShotChangedEventArgs() { }

        public ScreenShotChangedEventArgs(Bitmap currentScreenShot, Bitmap currentScreenShotDelta, int sequenceNumber)
        {
            SequenceNumber = sequenceNumber;
            CurrentScreenShot = currentScreenShot;
            CurrentScreenShotDelta = currentScreenShotDelta;
        }
    }
    

    // make this also responsible for recombining the delta
    public class UpdateScreenshotService
    {
        public event EventHandler<ScreenShotChangedEventArgs> ScreenShotUpdated;
        public event EventHandler<ScreenShotErrorEventArgs> Error;

        public string SessionID { get; set; }
        public string ServiceUrl { get; set; }

        public int SequenceNumber { get; set; }     // 0 means need initial screenshot
        private string SequenceID { get; set; }     // group series of images together, create a new sequence ID if a sequence of updates is corrupted (eg dropped update or arrive out-of-order)

        private Bitmap CurrentScreenShot { get; set; }
        private Bitmap CurrentScreenShotDelta { get; set; }

        private ICompressBitmapStrategy BitmapCompressor { get; set; }

        private ProducerConsumerQueue<Tuple<Bitmap, string, int>> OutgoingScreenshotsQueue { get; set; }

        private int networkErrors = 0;
        private const int MAX_ERRORS_PER_RETRY = 3;
        private const int MAX_FRAMES_IN_QUEUE = 10;
        
        private readonly object syncLock = new object();

        private const int TIMEOUT_DEFAULT = 5000;
        private int _serviceTimeout = 0;
        public int ServiceTimeout
        {
            get
            {
                return _serviceTimeout > 0 ? _serviceTimeout : int.TryParse(ConfigurationManager.AppSettings["SERVICE_TIMEOUT"], out _serviceTimeout) ? _serviceTimeout : TIMEOUT_DEFAULT;
            }
        }

        public UpdateScreenshotService(string serviceUrl, string sessionID, ICompressBitmapStrategy bitmapCompressor)
        {
            SessionID = sessionID;
            ServiceUrl = serviceUrl;
            // has to be 1 or else threading will cause messages to arrive out of order
            OutgoingScreenshotsQueue = new ProducerConsumerQueue<Tuple<Bitmap, string, int>>(ProcessScreenshotUpdate, 1);
            BitmapCompressor = bitmapCompressor;
        }

        public void InitializeNewSequence(string sequenceID = null)
        {
            lock (syncLock)
            {
                OutgoingScreenshotsQueue.ClearQueue();
                CurrentScreenShot = null;
                CurrentScreenShotDelta = null;
                SequenceNumber = 0;
                SequenceID = sequenceID;
            }
        }

        public bool UpdateScreenshot(Bitmap bmpDelta)
        {
            if (OutgoingScreenshotsQueue.Count() < MAX_FRAMES_IN_QUEUE)
            {
                lock (syncLock)
                {
                    OutgoingScreenshotsQueue.EnqueueItem(new Tuple<Bitmap, string, int>(bmpDelta, SequenceID, SequenceNumber++));
                }
            }
            else
            {
                // if we have too many unprocessed frames in the current sequence, we can't drop frames
                // so force resending of new image sequence
                Console.WriteLine("Queue capacity exceeded - reverting to sending entire screen");
                InitializeNewSequence();
            }
            
            return true;
        }

        public WebRequest CreateRequest(int x, int y, int width, int height, string sequenceId, int sequence, byte[] payload, string serviceUrl = null)
        {
            serviceUrl = string.IsNullOrWhiteSpace(serviceUrl) ? (ServiceUrl + "/" + SessionID) : serviceUrl;
            WebRequest webRequest = HttpWebRequest.Create(serviceUrl);
            webRequest.ContentType = "application/octet-stream";
            webRequest.Method = "POST";
            webRequest.Timeout = 5000;
            webRequest.Headers.Add("rect_x", x.ToString());
            webRequest.Headers.Add("rect_y", y.ToString());
            webRequest.Headers.Add("rect_width", width.ToString());
            webRequest.Headers.Add("rect_height", height.ToString());
            webRequest.Headers.Add("sequence_id", sequenceId);
            webRequest.Headers.Add("sequence", sequence.ToString());
            
            Stream reqStream = webRequest.GetRequestStream();
            reqStream.Write(payload, 0, payload.Length);

            return webRequest;
        }

        public void ProcessScreenshotUpdate(Tuple<Bitmap, string, int> request)
        {
            lock (syncLock)
            {
                DateTime dtStart = DateTime.Now;
                Bitmap currentScreenShot = request.Item1;
                string sequenceId = request.Item2;
                int sequenceNumber = request.Item3;

                if (networkErrors >= MAX_ERRORS_PER_RETRY)
                {
                    OnError("Maximum network errors count exceeded");
                    return;
                }

                System.Drawing.Rectangle rectChanges = BitmapHelpers.GetBoundingBoxForChanges(CurrentScreenShot, currentScreenShot);
                if (rectChanges == System.Drawing.Rectangle.Empty)
                {
                    // nothing changed so don't bother updating for this frame
                    return;
                }

                // extract only the changed part
                CurrentScreenShotDelta = BitmapHelpers.CropBitmap(currentScreenShot, rectChanges);

                // update the complete image
                CurrentScreenShot = sequenceNumber == 0 ? CurrentScreenShotDelta : BitmapHelpers.CopyBitmap(CurrentScreenShot, rectChanges, CurrentScreenShotDelta);
                OnScreenShotUpdated(CurrentScreenShot, CurrentScreenShotDelta, sequenceNumber);
                Console.WriteLine(string.Format("Changed area: TOP LEFT ({0}, {1}) [{2}x{3}]", rectChanges.X, rectChanges.Y, rectChanges.Width, rectChanges.Height));

                try
                {
                    byte[] compressedBytes = BitmapCompressor.CompressBitmap(CurrentScreenShotDelta);
                    WebRequest webRequest = CreateRequest(rectChanges.X, rectChanges.Y, rectChanges.Width, rectChanges.Height, SequenceID, SequenceNumber, compressedBytes);
                    
                    webRequest.BeginGetResponse(result =>
                    {
                        using (HttpWebResponse webResp = (HttpWebResponse)webRequest.EndGetResponse(result))
                        {
                            lock (syncLock)
                            {
                                //string sequence_id = webResp.Headers["sequence_id"];
                                switch (webResp.StatusCode)
                                {
                                    case HttpStatusCode.Created:
                                        // 201 - acknowledged the start of a new sequence OR accepted an in-order update
                                        Console.WriteLine(string.Format("Seq: {0} | {1} bytes uploaded in {2} ms", SequenceID, compressedBytes.Length, (DateTime.Now - dtStart).Milliseconds));
                                        networkErrors = 0;
                                        break;
                                    case HttpStatusCode.NotModified:
                                        // 304 - start-over; the previous sequence was corrupted
                                        InitializeNewSequence();
                                        break;
                                    case HttpStatusCode.BadRequest:
                                        // 400 - somehow got into an inconsistent state - start-over but mark as error and increment count in case it loops infinitely
                                        InitializeNewSequence();
                                        networkErrors++;
                                        break;
                                    default:
                                        string error = string.Format("ERROR: unknown server state: {0} {1}", webResp.StatusCode, webResp.StatusDescription);
                                        Console.WriteLine(error);
                                        OnError(error);
                                        break;
                                }
                            }
                        }
                    }, null);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    networkErrors++;
                }
            }
        }

        protected void OnScreenShotUpdated(Bitmap currentScreenShot, Bitmap currentScreenShotDelta, int sequenceNumber)
        {
            var handlers = ScreenShotUpdated;
            if (handlers != null)
            {
                handlers(this, new ScreenShotChangedEventArgs(currentScreenShot, currentScreenShotDelta, sequenceNumber));
            }
        }

        protected void OnError(string error)
        {
            var handlers = Error;
            if (handlers != null)
            {
                handlers(this, new ScreenShotErrorEventArgs(error));
            }
        }
    }
}
