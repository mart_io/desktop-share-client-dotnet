﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using mart.io.DesktopShareClientDotNet.Interfaces;
using System.Drawing;
using mart.io.DesktopShareClientDotNet.Utils;

namespace mart.io.DesktopShareClientDotNet.Services
{
    public class DesktopScreenshotProvider : IScreenshotProvider
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        private DesktopScreenshotProvider() 
        { 
        }

        public static IScreenshotProvider CreateScreenshotProvider(int x, int y, int width, int height) 
        {
            return new DesktopScreenshotProvider() { X = x, Y = y, Width = width, Height = height };
        }

        public Bitmap TakeScreenshot()
        {
            return ScreenshotUtility.GetScreenshot(X, Y, Width, Height);
        }
    }
}
