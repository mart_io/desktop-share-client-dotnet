﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using mart.io.DesktopShareClientDotNet.Interfaces;
using System.Drawing;
using mart.io.DesktopShareClientDotNet.Utils;

namespace mart.io.DesktopShareClientDotNet.Services
{
    public class WindowScreenshotProvider : IScreenshotProvider
    {
        public IntPtr WindowHandle { get; set; }

        private WindowScreenshotProvider()
        {
        }

        public static IScreenshotProvider CreateScreenshotProvider(IntPtr windowHandle)
        {
            return new WindowScreenshotProvider() { WindowHandle = windowHandle };
        }

        public Bitmap TakeScreenshot()
        {
            return ScreenshotUtility.GetScreenshot(WindowHandle);
        }
    }
}
