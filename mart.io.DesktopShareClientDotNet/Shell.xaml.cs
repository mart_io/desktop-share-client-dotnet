﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Timers;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Interop;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using System.Net;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using System.Configuration;
using Forms = System.Windows.Forms;

using Microsoft.Practices.Prism.Commands;
using mart.io.DesktopShareClientDotNet.Utils;
using mart.io.DesktopShareClientDotNet.Interfaces;
using mart.io.DesktopShareClientDotNet.Services;
using mart.io.DesktopShareClientDotNet.Models;
using mart.io.DesktopShareClientDotNet.Util;

namespace mart.io.DesktopShareClientDotNet
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Shell : Window, INotifyPropertyChanged
    {
        public ObservableCollection<DisplayInfoView> DetectedDisplays { get; set; }
        public ObservableCollection<Process> OpenWindows { get; set; }

        public UpdateScreenshotService UpdateScreenshotService { get; set; }

        private ImageBrush _currentImageBrush;
        public ImageBrush CurrentImageBrush
        {
            get
            {
                return _currentImageBrush;
            }
            set
            {
                if (value != this._currentImageBrush)
                {
                    this._currentImageBrush = value;
                    NotifyPropertyChanged("CurrentImageBrush");
                }
            }
        }

        private String _statusLabelText;
        public String StatusLabelText
        {
            get
            {
                return string.IsNullOrEmpty(TooltipText) ? _statusLabelText : TooltipText;
            }
            set
            {
                if (value != this._statusLabelText)
                {
                    this._statusLabelText = value;
                    NotifyPropertyChanged("StatusLabelText");
                }
            }
        }

        private String _tooltipText;
        public String TooltipText
        {
            get
            {
                return _tooltipText;
            }
            set
            {
                if (value != this._tooltipText)
                {
                    this._tooltipText = value;
                    NotifyPropertyChanged("StatusLabelText");
                }
            }
        }

        private String _sessionID;
        public String SessionID
        {
            get
            {
                return _sessionID;
            }
            set
            {
                if (value != this._sessionID)
                {
                    this._sessionID = value;
                    NotifyPropertyChanged("SessionID");
                }
            }
        }

        private bool _isSharingCanvasVisible;
        public bool IsSharingCanvasVisible
        {
            get
            {
                return _isSharingCanvasVisible;
            }
            set
            {
                if (value != this._isSharingCanvasVisible)
                {
                    this._isSharingCanvasVisible = value;
                    NotifyPropertyChanged("IsSharingCanvasVisible");
                }
            }
        }

        private bool _isSharingPaused;
        public bool IsSharingPaused
        {
            get
            {
                return _isSharingPaused;
            }
            set
            {
                if (value != this._isSharingPaused)
                {
                    this._isSharingPaused = value;
                    NotifyPropertyChanged("IsSharingPaused");
                }
            }
        }

        private bool _isProcessListVisible;
        public bool IsProcessListVisible
        {
            get
            {
                return _isProcessListVisible;
            }
            set
            {
                if (value != this._isProcessListVisible)
                {
                    this._isProcessListVisible = value;
                    NotifyPropertyChanged("IsProcessListVisible");
                }
            }
        }

        private bool _isSettingsVisible;
        public bool IsSettingsVisible
        {
            get
            {
                return _isSettingsVisible;
            }
            set
            {
                if (value != this._isSettingsVisible)
                {
                    this._isSettingsVisible = value;
                    NotifyPropertyChanged("IsSettingsVisible");
                }
            }
        }

        private int _updatesPerSecond;
        public int UpdatesPerSecond
        {
            get
            {
                return _updatesPerSecond;
            }
            set
            {
                if (value != this._updatesPerSecond)
                {
                    this._updatesPerSecond = value;
                    NotifyPropertyChanged("UpdatesPerSecond");
                }
            }
        }

        private string _serviceUrl;
        public string ServiceUrl
        {
            get
            {
                return _serviceUrl;
            }
            set
            {
                if (value != this._serviceUrl)
                {
                    this._serviceUrl = value;
                    NotifyPropertyChanged("ServiceUrl");
                }
            }
        }

        public object[] ServiceUrls
        {
            get
            {
                return new object[] {
                    new { Text = "Local", Value = "http://localhost:3000/update_frame" },
                    new { Text = "mart.io", Value = "http://node.mart.io:5000/update_frame" },
                    new { Text = "Heroku", Value = "https://mart-io-desktopshare.herokuapp.com/update_frame" }
                };
            }
        }

        public ICommand PauseDesktopSharingCommand { get; set; }
        public ICommand UpdateTooltipTextCommand { get; set; }

        private IEnumerable<Process> FilteredProcesses
        {
            get
            {
                return Process.GetProcesses().Where(p => p.MainWindowHandle != IntPtr.Zero && !String.IsNullOrEmpty(p.MainWindowTitle));
            }
        }

        private IScreenshotProvider ScreenshotProvider { get; set; }
        private CaptureWindow CaptureWindow { get; set; }
        private MaskWindow SelectionRectangle { get; set; }
        private System.Timers.Timer DesktopShareTimer { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        public Shell()
        {
            SessionID = "ABC";
            ServiceUrl = ConfigurationManager.AppSettings["SERVICE_URL"];
            UpdatesPerSecond = Int32.TryParse(ConfigurationManager.AppSettings["INITIAL_UPDATES_PER_SECOND"], out _updatesPerSecond) ? _updatesPerSecond : 4;
            OpenWindows = new ObservableCollection<Process>(FilteredProcesses);
            DetectedDisplays = new ObservableCollection<DisplayInfoView>(Forms.Screen.AllScreens.Select((x, index) => new DisplayInfoView { Screen = x, ItemIndex = index + 1 }));

            UpdateScreenshotService = new Services.UpdateScreenshotService(ServiceUrl, SessionID, new PNGBitmapCompression());
            UpdateScreenshotService.ScreenShotUpdated += new EventHandler<ScreenShotChangedEventArgs>(OnUpdateScreenshotServiceScreenShotUpdated);
            UpdateScreenshotService.Error += new EventHandler<ScreenShotErrorEventArgs>(OnUpdateScreenshotServiceNetworkError);

            DesktopShareTimer = new System.Timers.Timer();
            UpdateTooltipTextCommand = new DelegateCommand<string>(UpdateTooltipText);
            PauseDesktopSharingCommand = new DelegateCommand<bool?>(PauseDesktopSharing);

            Loaded += new RoutedEventHandler(Shell_Loaded);
            InitializeComponent();
        }

        private void Shell_Loaded(object sender, RoutedEventArgs e)
        {
            Border titleBar = this.Template.FindName("PART_TITLEBAR", this) as Border;
            if (titleBar != null)
            {
                titleBar.MouseLeftButtonDown += PART_TITLEBAR_MouseLeftButtonDown;
                Button btnClose = titleBar.FindName("PART_CLOSE") as Button;
                if (btnClose != null)
                {
                    btnClose.Click += PART_CLOSE_Click;
                }
            }
            CaptureWindow = new CaptureWindow() { Owner = this, Visibility = Visibility.Collapsed };
            SelectionRectangle = new MaskWindow() { Owner = this };
            SelectionRectangle.SelectionChanged += new EventHandler(SelectionRectangle_SelectionChanged);
        }

        private void UpdateTooltipText(string text)
        {
            TooltipText = text;
        }

        private void SelectionRectangle_SelectionChanged(object sender, EventArgs e)
        {
            ResetToolbar(false);
            IsSharingCanvasVisible = true;

            CaptureWindow.Left = SelectionRectangle.SelectionTopLeft.X;
            CaptureWindow.Top = SelectionRectangle.SelectionTopLeft.Y;
            CaptureWindow.Width = SelectionRectangle.SelectionArea.Width;
            CaptureWindow.Height = SelectionRectangle.SelectionArea.Height;
            CaptureWindow.Show();
            CaptureWindow.LocationChanged += new EventHandler(CaptureWindow_LocationChanged);

            CaptureWindow_LocationChanged(CaptureWindow, EventArgs.Empty);
        }

        private void CaptureWindow_LocationChanged(object sender, EventArgs e)
        {
            ScreenshotProvider = DesktopScreenshotProvider.CreateScreenshotProvider(
                (int)CaptureWindow.Left,
                (int)CaptureWindow.Top,
                (int)CaptureWindow.Width,
                (int)CaptureWindow.Height);
            StartScreenshotService();
        }

        private void OnUpdateScreenshotServiceScreenShotUpdated(object sender, ScreenShotChangedEventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                try
                {
                    WriteableBitmap bitmap = (WriteableBitmap)BitmapHelpers.RawBitmapToBitmapSource(e.CurrentScreenShot);
                    if (bitmap != null)
                    {
                        CurrentImageBrush = new ImageBrush(bitmap);
                        bitmap = null;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    StatusLabelText = "An error occurred attempting to redraw the updated screenshot";
                }
            }));
        }

        private void OnUpdateScreenshotServiceNetworkError(object sender, ScreenShotErrorEventArgs e)
        {
 	        Application.Current.Dispatcher.Invoke((Action)(() =>
            {
                chkPlayStop.IsChecked = false;
                StatusLabelText = string.Format("ERROR: {0}", e.Error);
                Console.WriteLine(string.Format("ERROR: {0}", e.Error));
                IsSharingPaused = true;
                DesktopShareTimer.Stop();
            }));
        }

        private void StartScreenshotService()
        {
            DesktopShareTimer.Stop();
            DesktopShareTimer = new System.Timers.Timer();
            DesktopShareTimer.Interval = 1000 / UpdatesPerSecond;

            DesktopShareTimer.Elapsed += (o, arg) =>
            {
                SendScreenshot(ScreenshotProvider);
            };
            DesktopShareTimer.Start();
            chkPlayStop.IsChecked = true;
        }

        private void SendScreenshot(IScreenshotProvider screenshotProvider)
        {
            Bitmap currentScreenShot = null;
            
            currentScreenShot = screenshotProvider.TakeScreenshot();
            if (currentScreenShot == null) return;
            UpdateScreenshotService.UpdateScreenshot(currentScreenShot);
        }

        private void ProcessSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ResetToolbar(false);
            IsSharingCanvasVisible = true;
            Process proc = lbProcesses.SelectedItem as Process;
            if (proc == null) return;
            ScreenshotProvider = WindowScreenshotProvider.CreateScreenshotProvider(proc.MainWindowHandle);
            StartScreenshotService();
        }

        // full screen
        private void ChangeDisplaySelection(object sender, RoutedEventArgs e)
        {
            ResetToolbar(false);
            IsSharingCanvasVisible = true;
            var selectedDisplay = ((DisplayInfoView)(((RadioButton)sender).DataContext)).Screen;
            if (selectedDisplay == null) {
                StatusLabelText = "Display is no longer valid";
                return; 
            }
            ScreenshotProvider = DesktopScreenshotProvider.CreateScreenshotProvider(
                selectedDisplay.WorkingArea.X,
                selectedDisplay.WorkingArea.Y,
                selectedDisplay.WorkingArea.Width,
                selectedDisplay.WorkingArea.Height);
            StartScreenshotService();
        }

        private void btnSelectRectangle_Click(object sender, RoutedEventArgs e)
        {
            SelectionRectangle.Show();
        }

        private void ShowSettings(object sender, RoutedEventArgs e)
        {
            StatusLabelText = string.Empty;
            ResetToolbar(true);
            IsSettingsVisible = true;
        }

        private void ShowProcesses(object sender, RoutedEventArgs e)
        {
            ResetToolbar(true);
            IsProcessListVisible = true;
        }

        private void ResetToolbar(bool stopSharing)
        {
            CaptureWindow.Hide();
            IsSharingCanvasVisible = false;
            IsProcessListVisible = false;
            IsSettingsVisible = false;

            if (stopSharing)
            {
                DesktopShareTimer.Stop();
            }
        }

        private void PauseDesktopSharing(bool? pause)
        {
            if (pause.Value)
            {
                DesktopShareTimer.Stop();
                StatusLabelText = "Paused";
                IsSharingPaused = true;
            }
            else
            {
                StartScreenshotService();
                StatusLabelText = "Sharing ...";
                IsSharingPaused = false;
            }
        }
        
        private void chkPlayStop_MouseEnter(object sender, MouseEventArgs e)
        {
            TooltipText = chkPlayStop.IsChecked.GetValueOrDefault() ? "Pause desktop sharing" : "Resume desktop sharing";
        }

        private void PART_TITLEBAR_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void PART_CLOSE_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
                DesktopShareTimer.Stop();
                Application.Current.Shutdown();
                System.Environment.Exit(0);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
